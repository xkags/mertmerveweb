﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public void DataDoldur()
        {
            using (var db = new CikolatanYanindaDB())
            {
                Musteri musteri1 = new Musteri();
                musteri1.MusteriAd = "Merve";
                musteri1.MusteriSoyad = "Aydın";
                musteri1.MusteriId = 1;
                db.Musteri.Add(musteri1);

                Urun urun1 = new Urun();
                urun1.UrunAd = "Fıstıklı Çikolata";
                urun1.UrunFiyat = 70;
                urun1.UrunId = 1;
                db.Urun.Add(urun1);

                db.SaveChanges();
                
            }
        }
    }
}