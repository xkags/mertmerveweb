namespace WebApplication1.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class CikolatanYanindaDB : DbContext
    {
        // Your context has been configured to use a 'CikolatanYanindaDB' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'WebApplication1.Models.CikolatanYanindaDB' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'CikolatanYanindaDB' 
        // connection string in the application configuration file.
        public CikolatanYanindaDB()
            : base("name=CikolatanYanindaDB")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        
         
           public DbSet<Musteri> Musteri { get; set; }
           public DbSet<Urun> Urun { get; set; }
        

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
    public class Musteri
    {
        public int MusteriId { get; set; }
        public string MusteriAd { get; set; }
        public string MusteriSoyad { get; set; }
        public int Sifre { get; set; }
    }

    public class Urun
    {
        public int UrunId { get; set; }
        public string UrunAd { get; set; }
        public int UrunFiyat { get; set; }

    }
    
}