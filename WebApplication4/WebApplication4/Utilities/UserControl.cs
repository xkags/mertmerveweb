﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication4.Utilities
{
    public static class UserOperations
    {
        public static bool IsLogged(HttpContextBase httpContext,string rUrl = null)
        {
            if (httpContext.Session["UserID"] != null && httpContext.Session["UserEmail"] != null)
            {
                return true;
            }
            else
            {
                if (rUrl != null)
                    httpContext.Response.Redirect("~/" + rUrl);
                return false;
            }
        }
    }
}