﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Utilities;

namespace WebApplication4.Controllers
{
    public class CartController : Controller
    {
        // GET: AlisverisSepet
        public ActionResult List()
        {
            if (!UserOperations.IsLogged(this.HttpContext, "User/Login"))
                return null;

            using (var context = new CYEntities())
            {
                int userID = Convert.ToInt32(Session["UserID"]);
                List<CartProduct> list = context.CartProducts.Include("Product").Where(p => p.UserID == userID && p.Status != 1).ToList();
                return View(list);
            }
        }
        [HttpPost]
        public ActionResult Add(int productID, int quantity)
        {
            if (!UserOperations.IsLogged(this.HttpContext, "User/Login"))
                return null;

            int userID = Convert.ToInt32(Session["UserID"]);
            using (var context = new CYEntities())
            {
                CartProduct cp = new CartProduct();
                cp.ProductID = productID;
                cp.Quantity = quantity;
                cp.UserID = userID;
                context.CartProducts.Add(cp);
                context.SaveChanges();
            }

            return RedirectToAction("List","Product");
        }
        [HttpPost]
        public ActionResult Remove(int id)
        {
            if (!UserOperations.IsLogged(this.HttpContext, "User/Login"))
                return null;

            using (var context = new CYEntities())
            {
                var cp = context.CartProducts.Where(p => p.ID == id).SingleOrDefault();
                if (cp != null)
                {
                    context.CartProducts.Remove(cp);
                    context.SaveChanges();
                }
            }

            return RedirectToAction("List");
        }
        [HttpPost]
        public ActionResult Update(int id,int quantity)
        {
            if (!UserOperations.IsLogged(this.HttpContext, "User/Login"))
                return null;

            using (var context = new CYEntities())
            {
                var cp = context.CartProducts.Where(p => p.ID == id).SingleOrDefault();
                cp.Quantity = quantity;
                context.SaveChanges();
            }

            return RedirectToAction("List");
        }
        public ActionResult Buy()
        {
            if (!UserOperations.IsLogged(this.HttpContext, "User/Login"))
                return null;
            using (var context = new CYEntities())
            {
                int userID = Convert.ToInt32(Session["UserID"]);
                List<CartProduct> list = context.CartProducts.Where(p => p.UserID == userID && p.Status != 1).ToList();
                if (list != null || list.Count > 0)
                {
                    foreach (var item in list)
                        item.Status = 1;

                    context.SaveChanges();
                }
            }

            return RedirectToAction("Success");
        }
        public ActionResult Success()
        {
            return View();
        }
    }
}