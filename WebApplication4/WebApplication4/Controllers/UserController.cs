﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Utilities;

namespace WebApplication4.Controllers
{
    public class UserController : Controller
    {
        // GET: Giris
        public ActionResult Login(string r)
        {
            ViewBag.Result = r;
            return View();
        }

        [HttpPost]
        public ActionResult Register(User model)
        {
            string r = "";
            if (String.IsNullOrWhiteSpace(model.Email) || String.IsNullOrWhiteSpace(model.Password) || String.IsNullOrWhiteSpace(model.Name))
                r = "Email, parola ve isim boş bırakılamaz.";
            else
            {
                using (var context = new CYEntities())
                {
                    model.Password = MD5.Hash(model.Password);
                    context.Users.Add(model);

                    context.SaveChanges();
                }
            }
            return RedirectToAction("Login", new { r = r });
        }
        [HttpPost]
        public ActionResult Login(User model)
        {
            string r = "";
            if (String.IsNullOrWhiteSpace(model.Email) || String.IsNullOrWhiteSpace(model.Password))
                r = "Email ve isim boş bırakılamaz.";
            else
            {
                using (var context = new CYEntities())
                {
                    model.Password = MD5.Hash(model.Password);
                    var user = context.Users.Where(p => p.Email == model.Email && p.Password == model.Password).FirstOrDefault();
                    if (user != null)
                    {
                        Session["UserID"] = user.ID;
                        Session["UserEmail"] = user.Email;
                        Session["UserName"] = user.Name;
                        Session["UserAddress"] = user.Address;

                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return RedirectToAction("Login", new { r = r });
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Account()
        {
            if (!UserOperations.IsLogged(this.HttpContext, "User/Login"))
                return null;

            using(var context = new CYEntities())
            {
                int userID = Convert.ToInt32(Session["UserID"]);
                User user = context.Users.Where(p => p.ID == userID).FirstOrDefault();
                return View(user);
            }
        }
        [HttpPost]
        public ActionResult Account(User model)
        {
            if (!UserOperations.IsLogged(this.HttpContext, "User/Login"))
                return null;

            if (String.IsNullOrWhiteSpace(model.Email) || String.IsNullOrWhiteSpace(model.Password) || String.IsNullOrWhiteSpace(model.Name))
                ViewBag.Error = "Email, parola ve isim boş bırakılamaz.";
            else
            {
                model.Password = MD5.Hash(model.Password);
                using (var context = new CYEntities())
                {
                    int userID = Convert.ToInt32(Session["UserID"]);
                    User user = context.Users.Where(p => p.ID == userID).FirstOrDefault();
                    user.Name = model.Name;
                    user.Password = model.Password;
                    user.Email = model.Email;
                    user.Address = model.Address;
                    context.SaveChanges();

                    Session["UserEmail"] = user.Email;
                    Session["UserName"] = user.Name;
                    Session["UserAddress"] = user.Address;
                }
            }

            return Account();
        }
    }
}