﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Utilities;

namespace WebApplication4.Controllers
{
    public class ProductController : Controller
    {
        // GET: Urunler
        public ActionResult List()
        {
            using (var context = new CYEntities()) {
                List<Product> list = context.Products.ToList();
                return View(list);
            }
        }
        
    }
}